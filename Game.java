import java.util.Scanner;

public class Game {

	private Player x;
	private Player o;
	private Board board;
	
	public Game() {
		x = new Player('X');
		o = new Player('O');
		board = new Board(x,o);
	}
	
	public void play() {
		showWelcome();
		while(true) {
			showTable();
			showTurn();
			input();
			if(board.isFinish()) {
				break;
			}
			board.switchPlayer();
		}
		showTable();
		showWinner();
		showStat();
	}
	private void showStat() {
		System.out.println(x.getName()+"W,D,L"+","+x.getWin()+","+x.getDraw()+","+x.getlose());
		System.out.println(o.getName()+"W,D,L"+","+o.getWin()+","+o.getDraw()+","+o.getlose());
	}
	private void showWinner() {
		Player player = board.getWinner();
		System.out.println(player.getName()+"Win ....");
	}
	
	private void showWelcome() {
		System.out.println("Welcome to game xo !!!");
		
	}
	
	private void showTable() {
		char[][] table = board.getTable();
		System.out.println(" 1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i+1);
			for (int j = 0; j < table.length; j++) {
				System.out.print(" "+ table[i][j]);
			}
			System.out.println();
		}
	}
	
	private void showTurn() {
		Player player = board.getCurrentPlayer();
		System.out.println(player.getName()+ " Turn :");
	}
	
	private void input() {
		Scanner sc = new Scanner(System.in);
		while(true) {
			try {
				System.out.print("Please input row and column : ");
				String input = sc.nextLine();
				String[] str = input.split(" ");
				if(str.length!=2) {
					System.out.println("Please input row and column [1-3] (ex: 1 2) ");
					continue;
				}
				int row = Integer.parseInt(str[0])-1;
				int col = Integer.parseInt(str[1])-1;
				if(board.setTable(row,col)==false) {
					System.out.println("Table is not empty !!");
					continue;
				}
				break;
			}catch (Exception e) {
				System.out.println("Please input row and column [1-3] (ex: 1 2) ");
				continue;
			}
			
		}
		
	}

}
